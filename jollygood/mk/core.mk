ifeq ($(PLATFORM), Windows)
	DEFS += -D_WIN32
	LIBS += -lole32 -lshlwapi
else
	DEFS += -DHAVE_LOCALE -DHAVE_LOCALTIME_R
	ifeq ($(PLATFORM), Darwin)
		DEFS += -DHAVE_STRTOF_L -DHAVE_SNPRINTF_L -DHAVE_XLOCALE \
			-DHAVE_STRLCPY -DHAVE_SETLOCALE
		LIBS += -framework CoreFoundation
	else ifeq ($(PLATFORM), NetBSD)
		DEFS += -DHAVE_POPCOUNT32 -DHAVE_SETLOCALE
	else
		DEFS += -DHAVE_USELOCALE
	endif
endif
